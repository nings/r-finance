
###################################################

library(quantmod)
getSymbols("USD/TWD",src="oanda")
chartSeries(USDTWD)
chartSeries(to.monthly(USDTWD))
addSAR()
addSMA()

###################################################

USDTWD_10Min = runMin(USDTWD, n = 10, cumulative = FALSE)
USDTWD_10Max = runMax(USDTWD, n = 10, cumulative = FALSE)
USDTWD_10Median = runMedian(USDTWD, n = 10, cumulative = FALSE)
USDTWD_10Mean = runMean(USDTWD, n = 10, cumulative = FALSE)
chartSeries(USDTWD)
addTA(USDTWD_10Min,on=1,type="l",col=2)
addTA(USDTWD_10Max,on=1,type="l",col=4)
addTA(USDTWD_10Median,on=1,type="l",col=5)
addTA(USDTWD_10Mean,on=1,type="l",col=6)


###################################################



